# ![Laravel Game Console]

This repo is functionality complete — PRs and issues welcome!

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.8/installation#installation)


Clone the repository

    git clone https://rupeshranjanmti@bitbucket.org/rupeshranjanmti/game.git

Switch to the repo folder

    cd game

Install all the dependencies using composer

    composer install

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

## Start Game

     php artisan team:result


## Test Case

Check the tests/Feature/GameTest.php

to run the test cases , please type the below command in terminal

     ./vendor/bin/phpunit
