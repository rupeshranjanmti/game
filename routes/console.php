<?php

use Lib\Game;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


Artisan::command('team:result', function () {
	$this->comment('Your game started.');

    $teamA = $this->ask('Enter Team A?');
    $teamB = $this->ask('Enter Team B?');

    $game = new Game($teamA, $teamB);
    
    $this->line($game->result());
});
