<?php

namespace Tests\Feature;

use Tests\TestCase;

class GameTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_console_command()
    {
        $input = [
            [
                'teamA' => '40, 20, 35, 50, 100',
                'teamB' => '35, 10, 30, 20, 90',
                'result' => 'Win'
            ],
            [
                'teamA' => '',
                'teamB' => '',
                'result' => 'Please enter both team\'s drain value'
            ],
            [
                'teamA' => '45, 20, 35, 10, 100',
                'teamB' => '35, 10, 70, 20, 90',
                'result' => 'Lose'
            ]
        ];

        foreach ($input as $value) {
            $this->artisan('team:result')
            ->expectsQuestion('Enter Team A?', $value['teamA'])
            ->expectsQuestion('Enter Team B?', $value['teamB'])
            ->expectsOutput($value['result'])
            ->assertExitCode(0);
        }
    }
}
