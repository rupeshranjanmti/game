<?php

namespace Lib;

class Game
{
    public $teamA;
    public $teamB;
    
    public function __construct($teamA, $teamB)
    {
        $this->teamA = $this->getTeam($teamA);
        $this->teamB = $this->getTeam($teamB);
    }

    private function getTeam($team)
    {
        return $team ? explode(',', $team) : null;
    }

    public function result()
    {
        if(! ($this->teamA && $this->teamB)){
            return "Please enter both team's drain value";
        }

        sort($this->teamA);
        foreach ($this->teamB as $key => $drain) {
            $resultArray[$key] = $this->checkTeamADrainWithTeamB($drain);

            if ($resultArray[$key] === false) {
                return 'Lose';
            }
        }

        return 'Win';
    }

    private function checkTeamADrainWithTeamB($drain)
    {
        $result = false;
        
        foreach ($this->teamA as $key => $teamDrain) {
            if ($teamDrain > $drain) {
                $result = $teamDrain;
                unset($this->teamA[$key]);
                break;
            }
        }
        return $result;
    }
}
